<head>
    <title>Tintoshoes</title>
    <link rel="stylesheet" href="assets/lib/bootstrap-5.1.3/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row mt-4">
        <div class="col-6 offset-3">
            <form action='upload.php' method='post' enctype='multipart/form-data' class='form-horizontal'>
                <div class='row mb-3'>
                    <div class="col">
                        <h5>Fecha de hoy: <?= date('d/m/Y') ?></h5>
                    </div>
                </div>
                <hr>
                <div class="row mb-3">
                    <div class='col'>
                        <label>Acumulado</label>
                        <input type='checkbox' name='acumulative' class="form-check" style="zoom:2">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class='col'>
                        <label>Dias de Credito</label>
                        <input type='number' name='days' min='0' class='form-control' placeholder='0'>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class='col'>
                        <label>Año</label>
                        <select name="year" class="form-control">
                            <?php foreach (glob(__DIR__ . '/config/year/*.json') as $year): ?>
                                <?php $year = basename($year, '.json') ?>
                                <option selected value="<?= $year ?>"><?= $year ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row mb-3">
                    <div class='col'>
                        <label>Archivo</label>
                        <input type='file' name='file' required class="form-control">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class='col offset-10'>
                        <button type='submit' name='submit' class='btn btn-primary'>Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>

