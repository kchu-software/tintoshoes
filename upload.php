<?php

if (isset($_POST['submit'])) {
    [
        "message" => $message,
        "proveedores" => $proveedores,
        "file" => $file
    ] = upload_file(
        $_POST['year'] ?? date('Y'),
        $days = !empty($_POST['days']) ? $_POST['days'] : null,
        $_POST['acumulative'] ?? null
    );
} else {
    header('Location: ./index.php');
}

?>
    <head>
        <title>Tintoshoes</title>
        <link rel='stylesheet' href='assets/lib/bootstrap-5.1.3/css/bootstrap.min.css'>
    </head>
    <body>
    <div class='container-fluid'>
        <div class='row mt-4'>
            <div class='col-6 offset-3'>
                <div class='row mb-3'>
                    <div class='col'>
                        <h5>===============================================================</h5>
                    </div>
                </div>
                <div class='row mb-3'>
                    <div class='col'>
                        <h5>Uploading...</h5>
                    </div>
                </div>
                <div class='row mb-3'>
                    <div class='col'>
                        <h5><?= $message ?></h5>
                    </div>
                </div>
                <div class='row mb-3'>
                    <div class='col'>
                        <h5>===============================================================</h5>
                    </div>
                </div>
                <div class='row mb-3'>
                    <div class='col'>
                        <h5>PROVEEDORES ENCONTRADOS: <?= count($proveedores) ?></h5>
                    </div>
                </div>
                <div class='row mb-3'>
                    <div class='col'>
                        <a href="<?= 'downloads/' . $file ?>" class='btn btn-link'>DESCARGAR: <?= $file ?></a>
                    </div>
                </div>
                <div class='row mb-3'>
                    <div class='col'>
                        <h5>===============================================================</h5>
                    </div>
                </div>
                <div class='row mb-3'>
                    <div class='col'>
                        <a href='index.php' class="btn btn-link">REGRESAR</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
<?php

function parse_date($date): string
{
    $date = str_replace('/', '-', $date);
    $valid = date_create_from_format('d-M-Y', $date);
    if (!$valid) {
        [, $month,] = explode('-', $date);
        $date = str_replace($month, ([
                'Ene' => 'Jan',
                'Abr' => 'Apr',
                'Ago' => 'Aug',
                'Dic' => 'Dec',
            ][$month] ?? $month), $date);
        $valid = date_create_from_format('d-M-Y', $date);
        if (!$valid) {
            throw new Exception('');
        }
    }
    return $date;
}

function upload_file($year, $days, $acumulative): array
{
    ini_set('file_uploads', true);
    setcookie('XDEBUG_SESSION', 'PHPSTORM');
    date_default_timezone_set('America/Mexico_City');
    include_once 'vendor/autoload.php';

    $target_dir = 'uploads/';
    $target_file = $target_dir . uniqid() . '.' . basename($_FILES['file']['name']);
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    if (!move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
        return ["message" => "Sorry, there was an error uploading your file."];
    }

    $file = htmlspecialchars(basename($_FILES['file']['name']));

    $parse = SimpleXLSX::parse($target_file);
    $rows = $parse->rows();

    $rows = array_slice($rows, 15);
    $headers = $rows[0];
    array_shift($rows);

    $txt_semanas = json_decode(file_get_contents(__DIR__ . '/config/year/' . $year . '.json'), true);

    if (!$txt_semanas) {
        throw new Exception("Missing config file " . __DIR__ . '/config/year/' . $year . '.json');
    }

    $id = null;
    $total_saldo = 0;
    $total_pagos = 0;
    $total = 0;
    $saldo_final = 0;
    $pagos_final = 0;
    $total_final = 0;
    $total_moneda = [
        'MXN' => 0,
        'USD' => 0,
    ];
    $total_semanas = [];
    $total_semanas_moneda = [
        'MXN' => [],
        'USD' => [],
    ];
    $proveedores = [];
    foreach ($rows as $row) {
        if (is_numeric($row[1])) {
            if (!empty($id)) {
                $proveedores[$id]['saldo'] = $total_saldo;
                $proveedores[$id]['pagos'] = $total_pagos;
                $proveedores[$id]['total'] = $total;
                $saldo_final += $total_saldo;
                $pagos_final += $total_pagos;
                $total_final += $total;
                $total_moneda[$proveedores[$id]['moneda']] += $total;
            }
            $total_saldo = 0;
            $total_pagos = 0;
            $total = 0;
            $id = $row[1];
            $proveedores[$id] = ['nombre' => $row[2]];
            continue;
        }
        if ($row[1] !== 'Compras') {
            continue;
        }

        $fecha_inicial = date('Y-m-d', strtotime(parse_date($row[6])));
        $fecha_final = $days ? date('Y-m-d', strtotime($fecha_inicial . " + $days days")) : date('Y-m-d', strtotime(parse_date($row[7])));
        $saldo = floatval(str_replace(',', '', $row[10]));
        $pago = floatval(str_replace(',', '', $row[11])) + floatval(str_replace(',', '', $row[12]));

        $semana = 0;
        $current_week = 0;
        for ($i = 1; $i <= count($txt_semanas); $i++) {
            if ($acumulative) {
                if ($current_week == 0 &&
                    date('Y-m-d') >= date('Y-m-d', strtotime($txt_semanas[$i]['start']))
                    && date('Y-m-d') <= date('Y-m-d', strtotime($txt_semanas[$i]['end']))
                ) {
                    $current_week = $i;
                }
            }
        }
        for ($i = 1; $i <= count($txt_semanas); $i++) {
            if ($acumulative) {
                if (
                    date('Y-m-d', strtotime($fecha_final)) >= date('Y-m-d', strtotime($txt_semanas[$i]['start']))
                    && date('Y-m-d', strtotime($fecha_final)) <= date('Y-m-d', strtotime($txt_semanas[$i]['end']))
                ) {
                    $semana = $i;
                    if ($semana < $current_week) {
                        $semana = $current_week;
                        break;
                    }
                    break;
                } else if (date('Y-m-d', strtotime($fecha_final)) < date('Y-m-d', strtotime($txt_semanas[$i]['start']))) {
                    $semana = $current_week;
                    break;
                }
            } else {
                if (
                    date('Y-m-d', strtotime($fecha_final)) >= date('Y-m-d', strtotime($txt_semanas[$i]['start']))
                    && date('Y-m-d', strtotime($fecha_final)) <= date('Y-m-d', strtotime($txt_semanas[$i]['end']))
                ) {
                    $semana = $i;
                    break;
                }
            }
        }

        if ($semana === 0) {
            $semana = date('Y', strtotime($fecha_final));
            if ($semana == $year) {
                $semana = date('Y', strtotime($fecha_inicial));
            }
        }

        $dias = floatval(date_diff(date_create($fecha_final), date_create($fecha_inicial))->format('%a'));

        $moneda = 'MXN';
        switch ($id) {
            case 82:
            case 281:
            case 93:
                $moneda = 'USD';
                break;
        }

        $proveedores[$id]['moneda'] = $moneda;
        $proveedores[$id]['semanas'][$semana]['compras'][] = [
            'fecha_inicial' => $fecha_inicial,
            'fecha_final' => $fecha_final,
            'dias' => $dias,
            'semana' => $semana,
            'saldo' => $saldo,
            'pagos' => $pago,
            'total' => $saldo - $pago
        ];

        $total_saldo += $saldo;
        $total_pagos += $pago;
        $total += $saldo - $pago;

        $total_semanas[$semana] = $total_semanas[$semana] ?? 0;
        $total_semanas[$semana] += $saldo - $pago;

        $total_semanas_moneda[$moneda][$semana] = $total_semanas_moneda[$moneda][$semana] ?? 0;
        $total_semanas_moneda[$moneda][$semana] += $saldo - $pago;
    }
    if (!empty($id)) {
        $proveedores[$id]['saldo'] = $total_saldo;
        $proveedores[$id]['pagos'] = $total_pagos;
        $proveedores[$id]['total'] = $total;
        $saldo_final += $total_saldo;
        $pagos_final += $total_pagos;
        $total_final += $total;
        $total_moneda[$proveedores[$id]['moneda']] += $total;
    }

    array_walk($proveedores, function (&$proveedor) {
        array_walk($proveedor['semanas'], function (&$semana) {
            $semana['saldo'] = 0;
            $semana['pagos'] = 0;
            $semana['total'] = 0;
            foreach ($semana['compras'] as $compra) {
                $semana['saldo'] += $compra['saldo'];
                $semana['pagos'] += $compra['pagos'];
                $semana['total'] += $compra['total'];
            }
        });
    });

    $excel = [
        ['', '', '', ''],
        ['NÚMERO', 'PROVEEDOR', 'MONEDA', 'SALDO'],
    ];

    ksort($total_semanas);
    ksort($total_semanas_moneda['MXN']);

    $excel[0] = array_merge($excel[0], array_keys($total_semanas));

    foreach ($total_semanas as $num_semana => $total_semana) {
        $excel[1][] = $txt_semanas[$num_semana]['label'] ?? '';
        $total_semanas[$num_semana] = number_format($total_semana, 2);
    }

    foreach ($total_semanas_moneda['MXN'] as $num_semana => $total_semana) {
        $total_semanas_moneda['MXN'][$num_semana] = number_format($total_semana, 2);
    }

    foreach ($total_semanas_moneda['USD'] as $num_semana => $total_semana) {
        $total_semanas_moneda['USD'][$num_semana] = number_format($total_semana, 2);
    }

    foreach ($proveedores as $numero => $proveedor) {
        switch ($numero) {
            case 95:
            case 98:
                continue 2;
        }

        $moneda = $proveedor['moneda'] == 'MXN' ? 'P' : 'Dólares';
        $row = [
            $numero,
            $proveedor['nombre'],
            $moneda,
            number_format($proveedor['total'], 2),
        ];
        foreach ($total_semanas as $num_semana => $null) {
            $row[] = ($proveedor['semanas'][$num_semana]['total'] ?? null) ? number_format($proveedor['semanas'][$num_semana]['total'], 2) : '';
            $total_semanas_moneda['USD'][$num_semana] = $total_semanas_moneda['USD'][$num_semana] ?? '';
        }

        $excel[] = $row;
    }
    ksort($total_semanas_moneda['USD']);
    $excel[] = array_merge(['', 'Total', '', number_format($total_final, 2)], $total_semanas);
    $excel[] = [];
    $excel[] = array_merge(['', 'Pesos', '', number_format($total_moneda['MXN'], 2)], $total_semanas_moneda['MXN']);
    $excel[] = array_merge(['', 'Dolares', '', number_format($total_moneda['USD'], 2)], $total_semanas_moneda['USD']);
    $xlsx = SimpleXLSXGen::fromArray($excel);
    $uniqid = date('YmdHi');
    $file = $year . ".REPORTE X SEM PROVEEDORES.$uniqid.xlsx";
    $xlsx->saveAs('downloads/' . $file);

    return ["message" => "The file $file has been uploaded.", "proveedores" => $proveedores, "file" => $file];
}
